let collection = [];

function print() {
    return collection;
}

function enqueue(item) {
    // Add an item to the end of the queue
   collection.push(item);
   return collection;
}

function dequeue() {
    // Remove and return the item from the front of the queue
    collection.shift();
    return collection;
}

function front() {
    // Get the front item of the queue
    if (collection.length > 0) {
        return collection[0];
    }
    return "Queue is empty";
}

function size() {
    // Get the size of the queue
    return collection.length;
}

function isEmpty() {
    // Check if the queue is empty
    return collection.length === 0;
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
