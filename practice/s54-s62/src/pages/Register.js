import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {

    const {user} = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [firstName,setFirstName] = useState("");
    const [lastName,setLastName] = useState("");
    const [email,setEmail] = useState("");
    const [mobileNo,setMobileNo] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [address,setAddress] = useState("");
    const [city,setCity] = useState("");
    const [province,setProvince] = useState("");
    const [zipCode, setZipCode] = useState("");
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(confirmPassword);
    console.log(address);
    console.log(city);
    console.log(province);
    console.log(zipCode);

    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('http://localhost:4000/users/register',{

        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({

            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNo: mobileNo,
            password: password,
            address: address,
            city: city,
            province: province,
            zipCode: zipCode,

        })
        })
        .then(res => res.json())
        .then(data => {

        //data is the response of the api/server after it's been process as JS object through our res.json() method.
        console.log(data);
        //data will only contain an email property if we can properly save our user.
        if(data){

            setFirstName('');
            setLastName('');
            setEmail('');
            setMobileNo('');
            setPassword('');
            setConfirmPassword('');
            

            alert("Thank you for registering!")

        } else {
            
            alert("Please try again later.")
        }

        })
    }
    

    useEffect(()=>{

        if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !=="" && confirmPassword !=="") && (password === confirmPassword) && (mobileNo.length === 11)){

            setIsActive(true)

        } else {

            setIsActive(false)

        }

    },[firstName,lastName,email,mobileNo,password,confirmPassword])

    return (

        (user.id !== null) ?
            <Navigate to="/courses" />
        :
            
        <Form onSubmit={(e) => registerUser(e)}
        >
        <h1 className="my-5 text-center">Register</h1>
        
              <Row className="mb-3">
                <Form.Group as={Col} controlId="formGridFirstName">
                  <Form.Label>First Name:</Form.Label>
                  <Form.Control type="text" placeholder="First Name" required 
                                value={firstName} 
                                onChange={e => {setFirstName(e.target.value)}}
        />
                </Form.Group>
        
                <Form.Group as={Col} controlId="formGridLastName">
                  <Form.Label>Last Name:</Form.Label>
                  <Form.Control type="text" placeholder="Last Name" required 
                            value={lastName} 
                            onChange={e => {setLastName(e.target.value)}}
         />
                </Form.Group>
              </Row>
        
              <Row className="mb-3">
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label>Email:</Form.Label>
                  <Form.Control type="email" 
                            placeholder="Enter Email" 
                            required 
                            value={email} 
                            onChange={e => {setEmail(e.target.value)}}
        />
                </Form.Group>
        
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Label>Password:</Form.Label>
                  <Form.Control type="password" placeholder="Password" 
        required 
                            value={password} 
                            onChange={e => {setPassword(e.target.value)}}
        />
                </Form.Group>
        
        < Form.Group as={Col} controlId="formGridConfirmPassword">
                            <Form.Label>Confirm Password:</Form.Label>
                            <Form.Control 
                            type="password" 
                            placeholder="Retype Password" 
                            required 
                            value={confirmPassword} 
                            onChange={e => {setConfirmPassword(e.target.value)}}/>
                        </Form.Group>
        
              </Row>

              <Form.Group className="mb-3" controlId="formGridAddress">
                <Form.Label>Mobile No.:</Form.Label>
                <Form.Control placeholder="Enter Mobile Number" type="number" 
                            required 
                            value={mobileNo} 
                            onChange={e => {setMobileNo(e.target.value)}}
         />
              </Form.Group>
        
        
              <Form.Group className="mb-3" controlId="formGridAddress">
                <Form.Label>Address</Form.Label>
                <Form.Control placeholder="Complete Address" type="text" 
                            required 
                            value={address} 
                            onChange={e => {setAddress(e.target.value)}}
         />
              </Form.Group>
        
              <Row className="mb-3">
                <Form.Group as={Col} controlId="formGridCity" >
                  <Form.Label>City</Form.Label>
                  <Form.Control type="text" placeholder="City" required value="City"  onChange={e => {setCity(e.target.value)}}
        />
                </Form.Group>
        
                <Form.Group as={Col} controlId="formGridState">
                  <Form.Label>Province</Form.Label>
                  <Form.Select defaultValue="Choose..." required value="province" onChange={e => {setProvince(e.target.value)}}>
                    <option>Choose...</option>
                    <option>Bulacan</option>
                    <option>Pampanga</option>
                    <option>NCR</option>
                    <option>Metro Manila</option>
                    <option>Tarlac</option>
                  </Form.Select>
                  
                </Form.Group>
        
                <Form.Group as={Col} controlId="formGridZip">
                  <Form.Label>Zip Code</Form.Label>
                  <Form.Control type="text" required value="Zip Code"  onChange={e => {setZipCode(e.target.value)}}
        />
                </Form.Group>
              </Row>
        

                {
                    isActive

                    ? <Button variant="primary" type="submit">Submit</Button>
                    : <Button variant="primary" disabled>Submit</Button>
                }
            </Form>
        
        )
}

