const Cart = require('../models/Cart'); // Import the Cart model

// Create a new cart for a user
const createCart = async (userId) => {
  try {
    const cart = new Cart({ user: userId, items: [] });
    await cart.save();
    return cart;
  } catch (error) {
    throw new Error(`Failed to create a cart: ${error}`);
  }
};

// Add a product to the user's cart
const addToCart = async (cartId, productId, quantity) => {
  try {
    const cart = await Cart.findById(cartId);
    if (!cart) {
      throw new Error('Cart not found');
    }

    // Check if the product is already in the cart
    const existingCartItem = cart.items.find((item) => item.product.toString() === productId);
    if (existingCartItem) {
      existingCartItem.quantity += quantity;
    } else {
      cart.items.push({ product: productId, quantity });
    }

    await cart.save();
    return cart;
  } catch (error) {
    throw new Error(`Failed to add to cart: ${error}`);
  }
};

// Remove a product from the user's cart
const removeFromCart = async (cartId, productId) => {
  try {
    const cart = await Cart.findById(cartId);
    if (!cart) {
      throw new Error('Cart not found');
    }

    // Filter out the item to remove
    cart.items = cart.items.filter((item) => item.product.toString() !== productId);

    await cart.save();
    return cart;
  } catch (error) {
    throw new Error(`Failed to remove from cart: ${error}`);
  }
};

// Get the user's cart and its items
const getCart = async (cartId) => {
  try {
    const cart = await Cart.findById(cartId).populate('items.product');
    if (!cart) {
      throw new Error('Cart not found');
    }
    return cart;
  } catch (error) {
    throw new Error(`Failed to get cart: ${error}`);
  }
};

// Export the cart controller functions
module.exports = {
  createCart,
  addToCart,
  removeFromCart,
  getCart,
};
