const Product = require('../models/Product.js')
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

module.exports.addProduct = async (req, res) => {
	const newProduct = new Product ({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	});

	await newProduct.save().then((product, error) => {
		if(error){
			return res.send("Please fill out all the details needed")
		}else{
			return res.send("Congratulations on creating your new product")
		}
	})
	.catch(error => res.send(error));
}

const activeProduct = async (req, res) => {
	try {
   		 const products = await Products.find({ isActive: true });
    		res.status(200).json(products);
  	} catch (error) {
    res.status(500).send('Internal Server Error');
  }

};

const singleProduct = async (req, res) => {
  try {
    const products = await Product.findOne({ _id: req.params.id, isActive: true });
    res.status(200).json(product);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

const updateProduct = async (req, res) => {
  try {
    const products = await Product.findOneAndUpdate(
      { _id: req.params.id },
      {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
      }
    );
    res.status(200).json({ message: 'Product updated successfully!' });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

const archiveProduct = async (req, res) => {
  try {
    const products = await Product.findOneAndUpdate({ _id: req.params.id }, { isActive: false });
    res.status(200).json({ message: 'Product archived successfully!' });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};


const activateProduct = async (req, res) => {
  try {
    const products = await Product.findOneAndUpdate({ _id: req.params.id }, { isActive: true });
    res.status(200).json({ message: 'Product activated successfully!' });
  } catch (err) {
    res.status(500).send('Internal Server Error');
  }
};

module.exports = {
  addProduct,
  activeProduct,
  singleProduct,
  updateProduct,
  archiveProduct,
  activateProduct,
};

