const express = require('express');
const auth = require('../auth.js')
const router = express.Router();

const userController = require('../controllers/userControllers.js');


// User reg endpoint -> kaninong room siya pupunta if gumana yung function na yon

// .get() pinupush sa link | all data gathered are reflected on the web link
// .post() sa head lumalabas

router.post('/register', userController.registerUser);

router.post('/login', userController.userLogin);

module.exports = router;