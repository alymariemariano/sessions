const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const authMiddleware = require('../middleware/authMiddleware');

// Create a new shopping cart for the user
router.post('/create', authMiddleware, async (req, res) => {
  try {
    const cart = await cartController.createCart(req.user._id);
    res.status(201).json(cart);
  } catch (error) {
    res.status(500).json({ error: 'Failed to create a shopping cart' });
  }
});

// Add a product to the user's shopping cart
router.post('/add/:productId', authMiddleware, async (req, res) => {
  try {
    const cart = await cartController.addToCart(req.user.cart, req.params.productId);
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ error: 'Failed to add a product to the cart' });
  }
});

// Remove a product from the user's shopping cart
router.delete('/remove/:productId', authMiddleware, async (req, res) => {
  try {
    const cart = await cartController.removeFromCart(req.user.cart, req.params.productId);
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ error: 'Failed to remove a product from the cart' });
  }
});

// Get the user's shopping cart
router.get('/', authMiddleware, async (req, res) => {
  try {
    const cart = await cartController.getCart(req.user.cart);
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ error: 'Failed to get the shopping cart' });
  }
});

module.exports = router;
