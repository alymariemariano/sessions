console.log ("Hello World!");

// [SECTION] Syntax, Statements and Comments
	// JS Statements usually end with semicolon (;)


	// Comments:

	// There two (2) types of comments:
		//1. The single line comment denoted by //, double slash
		// 2. The multiple-line comment denoted by (crtl+ shift ? or/). /**/. double slash and asterisk in the middle

// [Section] Variables
	// It is used to contain data
	// Any information that is used by an application is stored in what we call a "memory"

	// DECLARING VARIABLES - tells our devices that a variable name is created.

	let myVariableNumber; //camel casing, the first letter of the first word is always small, then the following is captil.

	//Naming convention
	console.log("myVariableNumber");
	console.log(myVariableNumber);
	/*let MyVariableNumber
	let my-variable-number
	let my_variable_numbe*/

	// Syntax
		// let,const,var variableName

	//Declaring and Initializing Variables

	let productName = "desktop computer";
	console.log(productName);

	let productPrice = 18999;
	console.log(productPrice);

	//constant, impossible to reassign

	const interest = 3.539;

	// Reassigning of Variable values
	productName = "laptop";
	console.log(productName);

	// reassigning variabls vs. Initializing variables

	// Declare the variable first (rule of thumb of js)

	myVariableNumber = 2023;
	console.log(myVariableNumber)

	// Multiple variable declarations

	let productCode = "DC017";
	const productBrand = 'Dell';

	console.log(productCode, productBrand);

	//[Section] Datatypes - klase ng value na nilalagay natin sa variable

		// Strings -text
			let country = "Philippines";
			let province = 'Metro Manila';

		// Concatenation Strings
			let fullAddress = province + ', ' + country;
			console.log(fullAddress);

			let greeting = 'I live in the' + country;
			console.log(greeting);

			let mailAddress ='Metro Manila \n \nPhilippines';
			console.log(mailAddress);

			let message ="John's employees went home early";
			console.log(message);

			message = 'John\'s employees went home early again';
			console.log(message);

		//Numbers
			let headcount = 26;
			console.log(headcount);

		//Decimal Numbers/Fraction
			let grade = 98.7;
			console.log(grade);

		//Exponential Notation
			let planetDistance = 2e10;
			console.log(planetDistance);

		// Combining text and strings;
			console.log("John's grade last quarter is " + grade);

		// Boolean - it returns true or false value
			let isMarried = false;
			let inGoodConduct = true;
			console.log("is Married: "+ isMarried);
			console.log("in GoodConduct: "+ inGoodConduct);

		//Arrays - written with [], collect values, considered to be plural, it carries 2 or more data
			let grades = [98.7, 92.1, 90.2, 94.6];
			console.log(grades);
			console.log(grades[0]);

			//Composed of different data types
			let details = ["John", "Smith", 32, true];
			console.log(details);

		//Objects - {}
		//Composed of 'key value/pair'

			let person = {
				fullName: "juan Dela Cruz",
				age: 35,
				isMarried: false,
				contact: ["0955138463541", "6509595"],
				address: {
					houseNumber: '345',
					city: 'Manila'
				}
			};
			// We only use [] to call the index number of the data from the array
			console.log(person);
			console.log(person.fullName);
			console.log(person.address.houseNumber);


			let arrays = [
					['hey', 'jey', 'jry'],
					['hey', 'jey', 'jry'],
					['hey', 'jey', 'jry'],
				];
			console.log(arrays);

			let myGrades = {
				firstGrading: 98.7,
				secondGrading: 92.7,
				thirdGrading: 90.2,
				fourthGrading: 94.6
			};

			console.log(myGrades);

			// Type of Operator
			console.log(typeof myGrades);
			console.log(typeof arrays);
			console.log(typeof greeting);


			//Null
			 let spouse= null;
			 let myNumber = 0;
			 let myString= '';

			 console.log(spouse);
			 console.log(myNumber);
			 console.log(myString);

			 //Undefind
			 let fullName;
			 console.log(fullName);