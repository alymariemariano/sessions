// CRUD (Create/Insert | Retrieve/Read | Update | Delete) Operations(MongoDB)

// CRUD Operations are the heart of any backend app.
//  Mastering CRUD Operations is essentail for any developers


// to connect
// > go to mongodb atlas > connect > copy connection string > paste on mongoDB compass

// [SECTION] Creating documents

/*
SYNTAX:

db.users.insertOne({object})

*/

db.users.insertOne({
firstName: 'Jane',
lastName: 'Doe',
age: 21,
contact: {
	phone: '09123456789',
	email: 'janedoe@gmail.com'
	},
	courses: ["CSS", "JS","Python"],
	department: "none"
});

db.users.insertOne({
firstName: 'Alyssa',
lastName: 'Mariano',
age: 23,
contact: {
	phone: '09123456789',
	email: 'alymariemariano@gmail.com'
	},
	courses: ["HTML","CSS", "JS","Python"],
	department: "none"
});

db.users.insertOne({
firstName: 'Test',
lastName: 'Test',
age: 0,
contact: {
	phone: '00000',
	email: 'test@gmail.com'
	},
	courses: [],
	department: "none"
});
/*
SYNTAX:

db.users.insertMany([{object}])

*/

db.users.insertMany([
{
firstName: 'Alyssa',
lastName: 'Mariano',
age: 23,
contact: {
	phone: '09123456789',
	email: 'alymariemariano@gmail.com'
	},
courses: ["HTML","CSS", "JS","Python"],
department: "none"
},

{
firstName: 'Stephen',
lastName: 'Hawking',
age: 76,
contact: {
	phone: '09123456789',
	email: 'stephen@gmail.com'
	},
courses: ["HTML","JS","Python"],
department: "none"

},

{
firstName: 'Neil',
lastName: 'Armstrong',
age: 56,
contact: {
	phone: '09123456789',
	email: 'naeilarmstrong@gmail.com'
	},
courses: ["React","Laravel","Sass"],
department: "none"

}
]);

// [SECTION] - Read/Retrieve
/*
SYNTAX:

db.users.findOne();
db.suers.findOne({field: value});

*/

db.users.findOne();

db.users.findOne({firstName: 'Stephen'});

/*
SYNTAX:

db.users.find();
db.suers.find({field: value});

*/

db.users.find({department: 'none'});

// multiple criteria

db.users.find({department: 'none', age: 76});




// SECTION - updating

/*
SYNTAX:

	db.collectionName.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
{firstName: 'Test'},
{
	$set: {
		firstName: 'Bill',
		lastName: 'Gates',
		age: 65,
		contact: {
			phone: '123456789',
			email: 'billgates@gmail.com'
		},
		courses: ['PHP', "Laravel", "HTML"],
		department: 'operations',
		status: 'active'
	}
}
);

// find the data

db.users.findOne({firstName: 'Bill'});
db.users.findOne({firstName: 'Test'});



db.users.find({"contact.email": "billgates@gmail.com"});

// [SECTION] update many collection

/*
SYNTAX:

db.collectionName.updateMany({criteria}, {$set: {field: value}});
*/

db.users.updateMany(
{department: 'none'},
{
	$set: {
		department: "HR"
	}
}
);

// [SECTION] Deleting a data

/*
SYNTAX:

*/

db.users.insert({
	firstName: 'Test'
});

// Delete single document

/*
SYNTAX:

db.users.deleteOne({criteria})
*/

db.users.deleteOne({
	firstName: 'Test'
});

// Delete Many

/*
SYNTAX:

db.users.deleteMany({criteria})
*/

db.users.insert({
	firstName: 'Bill'
});

db.users.deleteMany({firstName: "Bill"});
