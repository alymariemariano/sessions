let http = require('http');

// mock database
let directory = [
	{
		'name' : 'Brandon',
		'email': 'brandon@mail.com',
	},
	{
		'name': 'Jobert',
		'email': 'jobert@mail.com'
	}

];

http.createServer(function(req, res){

	if(req.url == "/users" && req.method == "GET"){
		res.writeHead(201, {'Content-type': 'application/json'});

		res.write(JSON.stringify(directory));
		res.end();
	};

		if(req.url == "/users" && req.method == "POST"){

		// declare and initialize a 'requestBody' variable to empty string;
		// This will act as a placeholder for the data/resources that we do have.
		let requestBody = "";
			console.log(requestBody);// at this point, requestBody is still empty.

			// Data is received from the client and is process in the "data" stream

		req.on('data', function(data){

			requestBody += data;
			console.log("this is from request.on(data)")
			console.log(requestBody);
		})
		// response end step - only runs after the request has completely been sent.
		req.on('end', function(){

			console.log(requestBody);
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			console.log(newUser);
			directory.push(newUser);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
}).listen(4000);

console.log('Server is running at localhost:4000');