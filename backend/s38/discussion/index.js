let http = require(`http`);

let port = 4000;

// create a server

let app = http.createServer(function (req, res) {
	
	if(req.url === "/items" && req.method === "GET"){

		// 200 is a status code for OK
		res.writeHead(200, {'Content-type' : 'text/plain'});
		// ends the response process
		res.end('Data retrieved from the database');
	};

	// The method "POST"
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {'Content-type' : 'text/plain'});
		res.end('Data to be sent to the database');
	}
});

// informs us if the server is runnning by printing our message
// yung parenthesis before the function ay parameters

/*
	1st argument - the port number to assign the server
	2nd argument - the callback/function to run when the server is running
*/
app.listen(port, () => console.log('Server is running at localhost: 4000'));