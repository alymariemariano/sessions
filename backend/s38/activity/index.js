let http = require(`http`);

let port = 4000;



let app = http.createServer(function (req, res) {

	//FRONT END = USE GET METHOD,1st process na rerequest ni user --> BACK END = READ method --> .find() to read the file in database --> DATABASE(DB) 

	//FRONT END = POST METHOD, means gusto ipasok ni user yung data na ininput niya sa database --> BACK END = CREATE method --> .insert () to insert the file in database --> DATABASE(DB) 

	//FRONT END = PUT METHOD, means gusto ipasok ni user yung data na ininput niya sa database --> BACK END = UPDATE method --> .update () to update the file in database --> DATABASE(DB) 

	
	if(req.url === "/" && req.method === "GET"){

		res.writeHead(200, {'Content-type' : 'text/plain'});
	
		res.end('Welcome to booking system');
	};

	if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {'Content-type' : 'text/plain'});
		res.end('Welcome to your profile!');
	};

	if(req.url == "/course" && req.method == "POST"){
		res.writeHead(200, {'Content-type' : 'text/plain'});
		res.end("Here's our courses available");
	};

	if(req.url == "/addcourse" && req.method == "POST"){
		res.writeHead(200, {'Content-type' : 'text/plain'});
		res.end("Add a course to our resources.");
	};

	if(req.url == "/updatecourse" && req.method == "PUT"){
		res.writeHead(200, {'Content-type' : 'text/plain'});
		res.end("Update a course to our resources.");
	};

	if(req.url == "/archivecourses" && req.method == "DELETE"){
		res.writeHead(200, {'Content-type' : 'text/plain'});
		res.end("Archive courses to our resources.");
	};

});


app.listen(port, () => console.log('Server is running at localhost: 4000'));