// 1. What directive is used by Node.js in loading the modules it needs?

	//"require" directive is used by Node.js in loading the modules it needs


// 2. What Node.js module contains a method for server creation?

	// http module contains a method for server creation.


// 3. What is the method of the http object responsible for creating a server using Node.js?

	//http.createServer() is responsible for creating a server using Node.js

// 4. What method of the response object allows us to set status codes and content types?
    
    //".writeHead()" allows us to set status codes and content types


// 5. Where will console.log() output its contents when run in Node.js?

	// console.log() will be shown in the TERMINAL when run in Node.js



// 6. What property of the request object contains the address's endpoint?

	// response contains the address's endpoint