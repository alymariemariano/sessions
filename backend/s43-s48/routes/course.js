const express= require("express"); //ito yung server na nagahhandle ng HTTP methods
const courseController = require("../controllers/course.js")
const auth = require("../auth.js");

// destructuring of verify and verifyAdmin
const {verify, verifyAdmin} = auth;
const router = express.Router();


// Create a course POST
router.post("/", verify, verifyAdmin, courseController.addCourse);

// GET all courses
router.get("/all", courseController.getAllCourses);

// GET all "ACTIVE" course
router.get("/", courseController.getAllActive);

// GET 1 "SPECIFIC" course using its ID
// ":" wildcard
router.get("/:courseId", courseController.getCourse);

// Updating a course (admin only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Archiving a course
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// Activating a course/unarchive
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);


router.post("/archives", verify, verifyAdmin, courseController.archives);





module.exports = router; //means pwede pa i-share sa iba pang pages/file