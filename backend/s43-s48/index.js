// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); //may capacity to share our data to different platforms. we can use someone else's server. example sa faceook, connected na rin sa insta. CROSS origin

require('dotenv').config();

const userRoutes = require('./routes/user'); // Allows access to routes defined within our application

const courseRoutes = require("./routes/course.js") //for new course

// [ SECTION ] Environment Setup
const port = 4000;

// [ SECTION ] Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Allows all resources to acces our backend application;
app.use(cors()); // CORS is a security feature implemented by web browsers to control requests to a different domain than the one the website came from.

// [ SECTION ] Database Connection
mongoose.connect('mongodb+srv://admin:admin123@cluster0.yj8jlo5.mongodb.net/s43-s48?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [ SECTION ] Backend Routes
app.use('/users', userRoutes);

// backend routes for course
app.use('/courses', courseRoutes);

// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${ process.env.PORT || port }`);
	});
};

module.exports = app; 

