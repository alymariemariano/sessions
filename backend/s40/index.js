// initial set up by default

const express = require("express");

const app = express();
const port = 4000;

// Middlewares ->
app.use(express.json());
// Allows your app to read date form forms
app.use(express.urlencoded({extended: true}));
// urlencoded accepts all different data types


// hindi kasama sa initial set up
//[SECTION] Routes
//GET METHOD
app.get("/", (req, res) => {
	res.send("hello world");
})

app.get("/hello", (req, res) => {
	res.send("Hello from /hello end point.");
})

// route expects to receive a POST method at the URI "/hello"
app.post("/hello", (req, res) => {
	res.send(`hello there, ${req.body.firstName} ${req.body.lastName}!`);
})

// POST route to register user
let users = [];

app.post("/signup", (req, res) => {
    console.log(req.body);

    let userExists = users.some(user => user.username === req.body.username);
    console.log(userExists);

    if (userExists) {
        res.send(`User ${req.body.username} is already registered!`);
    } else {
        if (req.body.username !== "" && req.body.password !== "") {
            users.push(req.body);
            res.send(`User ${req.body.username} has been successfully registered!`);
        } else {
            res.send("Please input BOTH username and password");
        }
    }
})


// Change password - put method

app.put("/change-password", (req, res) => {
    let message;

    if (users.length === 0) {
        message = "There is no registered user.";
    } else {
        const userIndex = users.findIndex(user => user.username === req.body.username);

        if (userIndex !== -1) {
            users[userIndex].password = req.body.password;
            message = `User ${req.body.username}'s password has been updated.`;
        } else {
            message = "User does not exist.";
        }
    }

    res.send(message);
})


//kasama sa initial set up
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}