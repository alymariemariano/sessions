// [SECTION] Comparison Query Operator

//  $gt / $gte operator 

// gt means greater than

/*
SYNTAX:

db.collectionName.find({field : {$gt : value}})
db.collectionName.find({field : {$gte : value}})

*/

db.users.find({age : {$gt : 50}});

//  $lt / $lte operator

// lt means less than

/*
SYNTAX:

db.collectionName.find({field : {$lt : value}})
db.collectionName.find({field : {$lte : value}})

*/

db.users.find({age : {$lte : 50}});

//  $ne operator

// ne means not equal
/*
SYNTAX:

db.collectionName.find({field : {$ne : value}})
*/

db.users.find({age : {$ne : 50}});

//  $in operator

// in means 
/*
SYNTAX:

db.collectionName.find({field : {$ni : value}})
*/

db.users.find({lastName : {$in : ["Hawking", "Doe"]}});
db.users.find({course : {$in : ["HTML", "React"]}});

// [SECTION] Logical Query Operator

/*
SYNTAX:

db.users.find({$or: [{fieldA: value}, {fieldB: value}]});
*/

db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

// or with gt
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

// $and operator

// for and operator, note that every criteria should be true, else it will return error
/*
SYNTAX:

db.users.find({$and: [{fieldA: value}, {fieldB: value}]});
*/

db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

// [SECTION] Field Projection

// Inclusion

/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 to denote that the field is being included.
    - Syntax
        db.users.find({criteria},{field: 1})
*/

db.users.find(
{
	firstName: "Jane"
},

{
	firstName: 1,
	lastName: 1,
	contact: 1
}
);

// exclusion

db.users.find(
{
	firstName: "Jane"
},

{
	contact: 0,
	department: 0
}
);

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 2,
	contact: 1,
	_id: 0,
}

)