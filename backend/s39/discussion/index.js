/*[SECTION] JS Synchronous vs. Asynchronous
-- By default JS is SYNCHRONOUS -> only one (1) statement can be executed at a time.*/

// JS Code Reading -> Top to Bottom, Left to Right

console.log('Hello');
// consoles.log("What's up");
console.log('Great');

// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code  "blocking"

// if Aynchronous, uunahin niya yungmabilis mag output
// Can be seen like sa mga social media accounts
console.log("hello world"); //1st output

/*for (let i = 0; i <= 1500; i++) {
	console.log(i); //3rd output
}*/

console.log("what's up"); //2nd output

/*[SECTION] Getting all posts
fetch("URL");

fetch("URL").then((response) => {});*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts")); //output ay promise, kasi alone lang, walang chain process. means maganda connection sa server.

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

// json() will convert response object/promise into JSON format
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())//promise to convert to raw data. JSON method. 
.then(json => console.log(json)) //after json() add pa ng chain process for results. nagkaoutput. yung promise ay naconvert na sa raw data. 


/*'async' and 'await' keywords are another approach that can eb used to achieve asynchronous code.*/

async function fetchData(){

	let result = await fetch("https://jsonplaceholder.typicode.com/posts");

	console.log(result);

	console.log(typeof result);


/*we can access the content of the response by directly accessing it's body property*/
	console.log(result.body);

	let json = result.json();
	console.log(json);
};

fetchData();

// [SECTION] getting a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response => response.json())
.then(json => console.log(json)); 

// [SECTION] Creating a post
/*
SYNTAX:

fetch("URL", options)
.then(response => {})
.then(response => {})

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] updating a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE", 
});
