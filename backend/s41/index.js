const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB Connection using mongoose
// by default
mongoose.connect("mongodb+srv://admin:admin123@cluster0.yj8jlo5.mongodb.net/Cluster0-to-do?retryWrites=true&w=majority", {
	useNewUrlParser :true,
	useUnifiedTopology: true
	// allows us to avoid any current or future errors while connecting to mongoDB
});

// Set notification for connection success or failure
let db = mongoose.connection;
// If error occured, output in console.
db.on("error", console.error.bind(console, "connection error."))

// If connection is successful, output in console.
db.once("open", () => console.log("We're connected to the cloud database."));

// [SECTION] Mongoose Schema - checks the data
// Schemas determines the structure of the documents to be written in the database.

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending",
		// default -> equips predefined values
	}
})

// [SECTION] Models - create a collection in the database for the tasks
const Task = mongoose.model("Task", taskSchema);



// middlewares
app.use(express.json()); //read data into json format
app.use(express.urlencoded({extended:true}))//read and accepts data from forms.

// TASK ROUTES

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    - If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
//CREATE TASK ROUTE
app.post("/tasks", (req, res) => {

	Task.findOne({
		name: req.body.name
	}).then((result, error) => {
		// if document was found or already existing
		if(result != null && result.name === req.body.name){
			return res.send("Duplicate task found!");
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error
				}else{ //if there is no error
					return res.status(201).send("New Task Created!");
				}
			})
		}
	})
})

// GET ALL TASK
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, error) => {
		// if error occur
		if(error){
			return console.error(error);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})



// port listening
if(require.main === module) {
	app.listen(port, () => console.log(`Server is running at ${port}`))
}

module.exports = app;