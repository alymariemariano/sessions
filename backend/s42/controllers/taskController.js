// ../ lalabas sa models folder
const Task = require("../models/Task.js");

module.exports.getAllTask = () =>{
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		//req.body.name === requestBody.name
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
		}else{
			return task;
		}
	})
};

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
		}else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return res.send("cannot find data with the provided id")
		}else{
			result.name = requestBody.name;
		}

		result.name = requestBody.name;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
				return res.send("There is an error saving the update.")
			}else{
				return updatedTask;
			}
		})
	})
}