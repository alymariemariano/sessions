const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// default endpoint
app.use("/tasks", taskRoute);


mongoose.connect("mongodb+srv://admin:admin123@cluster0.yj8jlo5.mongodb.net/Cluster0-to-do?retryWrites=true&w=majority", {
	useNewUrlParser :true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
// If error occured, output in console.
db.on("error", console.error.bind(console, "connection error."))

// If connection is successful, output in console.
db.once("open", () => console.log("We're connected to the cloud database."));


// Server listening
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}