const  Users = require('../models/User.js')
const bcrypt = require('bcrypt');

const registerUser = async (req, res) => {
	const hashPassword = await bcrypt.hash(req.body.password, 10);
	const newUser = new Users({
		email: req.body.email,
		password: hashPassword
	});

	return newUser.save().then((user, error) => {
		if(error){
			res.status(500).send('Internal server error');
		}else{
			res.status(201).send('User created!');
		}
	})
};

const userLogin = async (req, res) => {
  return Users.findOne({ email: req.body.email })
    .then((user) => {
      if (user === null) {
        return res.status(401).send('Invalid Credentials! Please Try Again!');
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(user) });
        } else {
          return res.send(false);
        }
      }
    })
    .catch((err) => err);
};



module.exports = {
	registerUser,
	userLogin

};

