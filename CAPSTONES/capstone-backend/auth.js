const jwt = require('jsonwebtoken');
const private = "E-CommerceAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id :  user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};
	return jwt.sign(data, private, {});
}

module.exports.verify = (req, res, next) => {

	// Token is retrieved from the request header
	// Authorization (Auth Tab) > Bearer Token

	console.log(req.headers.authorization);

	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth: "Failed, No Token!"})
	}else{
		console.log(token);

		token = token.slice(7, token.length);

		console.log(token);

		jwt.verify(token, secret, function(error, decodedToken){
			if(error){
				return res.send({
					auth: "Failed",
					message: error.message
				});
			}else{
				console.log(decodedToken);

				req.user = decodedToken;

				next();
			}
		})
	}
}

module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}