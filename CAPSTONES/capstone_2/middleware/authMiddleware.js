const jwt = require('jsonwebtoken');
const secret = 'E-CommerceAPI'; // Replace with your secret key
const Users = require('../models/User.js'); // Import your Mongoose User model

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
  const token = req.headers.authorization;

  if (typeof token === 'undefined') {
    return res.status(401).json({ auth: 'Failed, No Token!' });
  } else {
    const tokenValue = token.slice(7); // Remove "Bearer " from the token

    jwt.verify(tokenValue, secret, async (error, decodedToken) => {
      if (error) {
        return res.status(401).json({ auth: 'Failed', message: error.message });
      } else {
        try {
          const user = await Users.findById(decodedToken.id);
          if (!user) {
            return res.status(401).json({ auth: 'Failed', message: 'User not found' });
          }
          req.user = user;
          next();
        } catch (err) {
          return res.status(500).json({ auth: 'Failed', message: 'Server error' });
        }
      }
    });
  }
};

module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.status(403).json({ auth: 'Failed', message: 'Action Forbidden' });
  }
};
