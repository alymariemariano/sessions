const User = require('../models/User');
const bcrypt = require('bcrypt');
const authMiddleware = require('../middleware/authMiddleware');

module.exports.registerUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const existingUser = await User.findOne({ email });

    if (existingUser) {
      return res.status(400).json({ message: 'User already exists.' });
    }

    // Create a new user
    const newUser = new User({
      email : req.body.email,
      password: await bcrypt.hash(req.body.password, 10)
    });

    
    await newUser.save();

    // Return a success message
    res.status(201).json({ message: 'User registered successfully.' });
  } catch (error) {
    // Handle registration errors
    res.status(500).json({ error: 'Registration failed.' });
  }
};

module.exports.loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return res.status(401).json({ message: 'Invalid password.' });
    }

    const token = authMiddleware.createAccessToken(user);

    res.status(200).json({ token });
  } catch (error) {
    // Handle login errors
    res.status(500).json({ error: 'Login failed.' });
  }
};

module.exports.getUserProfile = async (req, res) => {
  try {
    const user = req.user;

    // Return the user's profile
    res.status(200).json({ user });
  } catch (error) {
    // Handle errors
    res.status(500).json({ error: 'Failed to retrieve user profile.' });
  }
};
