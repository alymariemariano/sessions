const Order = require('../models/Order');
const authMiddleware = require('../middleware/authMiddleware');

module.exports.createOrder = async (req, res) => {
  try {
    const user = req.user;
    const { products, totalAmount } = req.body;

    const newOrder = new Order({
      userId: user._id, // Set the user for the order
      products,
      totalAmount,
    });

    await newOrder.save();

    // Return a success message or order details
    res.status(201).json({ message: 'Order created successfully.', order: newOrder });
  } catch (error) {
    // Handle order creation errors
    res.status(500).json({ error: 'Order creation failed.' });
  }
};

module.exports.getOrderHistory = async (req, res) => {
  try {
    const user = req.user;

    // Retrieve the order history of the user
    const orders = await Order.find({ userId: user._id });

    res.status(200).json({ orders });
  } catch (error) {
    // Handle errors
    res.status(500).json({ error: 'Failed to retrieve order history.' });
  }
};
