const Product = require('../models/Product');
const authMiddleware = require('../middleware/authMiddleware');

module.exports.getAllProducts = async (req, res) => {
  try {
    const products = await Product.find();
    res.status(200).json(products);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve products.' });
  }
};

module.exports.getActiveProducts = async (req, res) => {
  try {
    const activeProducts = await Product.find({ isActive: true });
    res.status(200).json(activeProducts);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve active products.' });
  }
};

module.exports.getSingleProduct = async (req, res) => {
  try {
    const productId = req.params.productId;
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).json({ message: 'Product not found.' });
    }

    res.status(200).json(product);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve product.' });
  }
};

module.exports.createProduct = async (req, res) => {
  try {
    // Check if the user is an admin (handled by authMiddleware.verifyAdmin)
    const { name, description, price, isActive } = req.body;

    if (!name || !description || !price || isActive === undefined) {
      return res.status(400).json({ error: 'Incomplete product data.' });
    }

    const newProduct = new Product({
      name,
      description,
      price,
      isActive,
    });

    await newProduct.save();
    res.status(201).json({ message: 'Product created successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to create product.' });
  }
};


module.exports.updateProduct = async (req, res) => {
  try {
    // Check if the user is an admin (handled by authMiddleware.verifyAdmin)
    const productId = req.params.productId;
    const { name, description, price, isActive } = req.body;

    const updatedProduct = await Product.findByIdAndUpdate(
      productId,
      {
        name,
        description,
        price,
        isActive,
      },
      { new: true }
    );

    if (!updatedProduct) {
      return res.status(404).json({ message: 'Product not found.' });
    }

    res.status(200).json({ message: 'Product updated successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to update product.' });
  }
};

module.exports.archiveProduct = async (req, res) => {
  try {
    // Check if the user is an admin (handled by authMiddleware.verifyAdmin)
    const productId = req.params.productId;

    const archivedProduct = await Product.findByIdAndUpdate(
      productId,
      { isActive: false },
      { new: true }
    );

    if (!archivedProduct) {
      return res.status(404).json({ message: 'Product not found.' });
    }

    res.status(200).json({ message: 'Product archived successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to archive product.' });
  }
};

module.exports.activateProduct = async (req, res) => {
  try {
    // Check if the user is an admin (handled by authMiddleware.verifyAdmin)
    const productId = req.params.productId;

    const activatedProduct = await Product.findByIdAndUpdate(
      productId,
      { isActive: true },
      { new: true }
    );

    if (!activatedProduct) {
      return res.status(404).json({ message: 'Product not found.' });
    }

    res.status(200).json({ message: 'Product activated successfully.' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to activate product.' });
  }
};
