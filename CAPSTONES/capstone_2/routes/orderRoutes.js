// orderRoutes.js
const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const authMiddleware = require('../middleware/authMiddleware');

// Non-admin user check-out (create order)
router.post('/create', authMiddleware.verify, orderController.createOrder);

module.exports = router;
