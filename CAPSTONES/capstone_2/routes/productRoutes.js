// productRoutes.js
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const authMiddleware = require('../middleware/authMiddleware');

// Retrieve all products
router.get('/', productController.getAllProducts);

// Retrieve all active products
router.get('/active', productController.getActiveProducts);

// Retrieve single product
router.get('/:productId', productController.getSingleProduct);

// Create product (admin only)
router.post('/create', authMiddleware.verify, authMiddleware.verifyAdmin, productController.createProduct);

// Update product information (admin only)
router.put('/:productId', authMiddleware.verify, authMiddleware.verifyAdmin, productController.updateProduct);

// Archive product (admin only)
router.put('/archive/:productId', authMiddleware.verify, authMiddleware.verifyAdmin, productController.archiveProduct);

// Activate product (admin only)
router.put('/activate/:productId', authMiddleware.verify, authMiddleware.verifyAdmin, productController.activateProduct);

module.exports = router;
