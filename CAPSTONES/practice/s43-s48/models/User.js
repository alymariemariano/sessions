//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    address : {
        type : String, 
        required : [true, "Addresss is required"]
    },
    // The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
    
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);