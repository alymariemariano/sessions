const express = require('express');

const productControllers = require('../controllers/productControllers.js')

const router = express.Router();

const {verify, verifyAdmin} = auth;

router.post('/add', verify, verifyAdmin, productControllers.addProduct);

router.get('/active', productControllers.activeProducts);

router.get('/:id/view', verify, productControllers.singleProduct);

router.get('/:id/update', verify, productControllers.updateProduct);

router.get('/:id/archive', verify, productControllers.archiveProduct);

router.get('/:id/activate', verify, productControllers.activateProduct);

module.exports = router;