import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import CourseCard from '../components/CourseCard';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "Sea-Las Purified Drinking Water",
        content: "Purity in Every Drop",
        /* buttons */
        destination: "/courses",
        label: "Buy now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}