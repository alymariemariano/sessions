const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required: [true, "Please add a user ID"]
	},

	product : [{
		productId : {
			type: String,
			required: [true, "Add a product ID"]
		},
		quantity : {
			type : String,
			required: [true, "Please add a quantity"]
		}
	}],

	totalAmount: {
		type: Number,
		required: [true, "Add total amount"]
	},

	createdOn : {
		type: Date,
		default: new Date()
	},
});

module.exports = mongoose.model('Orders', orderSchema);