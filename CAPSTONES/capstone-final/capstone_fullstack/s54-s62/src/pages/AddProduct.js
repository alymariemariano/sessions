import { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {
    const navigate = useNavigate();
    const { user } = useContext(UserContext);

    // Input states
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    function addProduct(e) {
        e.preventDefault();

        // Adjust the API endpoint to your backend's product creation endpoint.
        fetch('http://localhost:4000/products/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                // Additional fields like image or category can be added here.
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                if (data) {
                    Swal.fire({
                        icon: 'success',
                        title: 'New Product Added',
                    });

                    // Clear the form fields after a successful product addition.
                    setName('');
                    setDescription('');
                    setPrice(0);

                    navigate('/products'); // Redirect to the products page.
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Unsuccessful Product Creation',
                        text: data.message,
                    });
                }
            });
    }

    return user.isAdmin ? (
        <>
            <h1 className="my-5 text-center">Add Product</h1>
            <Form onSubmit={(e) => addProduct(e)}>
                <Form.Group>
                    <Form.Label>Name:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Name"
                        required
                        value={name}
                        onChange={(e) => {
                            setName(e.target.value);
                        }}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Description"
                        required
                        value={description}
                        onChange={(e) => {
                            setDescription(e.target.value);
                        }}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Price:</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter Price"
                        required
                        value={price}
                        onChange={(e) => {
                            setPrice(e.target.value);
                        }}
                    />
                </Form.Group>
                {/* Additional fields can be added here. */}
                <Button variant="primary" type="submit" className="my-5">
                    Submit
                </Button>
            </Form>
        </>
    ) : (
        <Navigate to="/products" />
    );
}
