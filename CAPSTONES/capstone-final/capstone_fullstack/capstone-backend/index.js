const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require('dotenv').config();

const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require("./routes/productRoutes.js");
const ordersRoutes = require('./routes/orderRoutes.js');

const port =4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use(cors());

mongoose.connect('mongodb+srv://admin:admin123@cluster0.yj8jlo5.mongodb.net/Capstone2_API?retryWrites=true&w=majority' , {
	useNewUrlParser: true, 
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error."))
db.once("open", () => console.log("We're connected to the cloud database."));

// [SECTION] BACK END ROUTES
app.use('/users', userRoutes);
app.use('/products', productRoutes);

if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${process.env.PORT || port}`)
	});
};

module.exports = app;


// index routes > building | routes router > room | controller > person