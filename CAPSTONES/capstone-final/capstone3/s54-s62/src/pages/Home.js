import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import CourseCard from '../components/CourseCard';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "Sea-La's Purified Drinking Water",
        content: "Your source of pure wellness",
        /* buttons */
        destination: "/courses",
        label: "Buy Now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}